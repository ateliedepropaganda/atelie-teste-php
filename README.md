# Teste Programador Backend PHP

## Cenário

O Ateliê acaba de receber uma nova conta, e o cliente solicitou um sistema de “gerenciamento de campanhas” onde seja possível cadastrar clientes, suas campanhas e os participantes de cada campanha.

## Teste:

 - Modelar base dados conforme estrutura descrita (disponibilizar dump SQL)
 - Criar uma API REST para criar, editar, listar e deletar clientes
 - Criar uma API REST para criar, editar, listar e deletar campanhas
 	- As campanhas devem ser atribuídas a um cliente específico
 - Criar uma API REST para criar, editar, listar e deletar participantes
 	- Os participantes devem ser atribuídos a uma campanha específica

O sistema deverá validar os campos, e não permitir cadastro duplicados de clientes
O sistema deverá validar os campos, e não permitir cadastro duplicados de participantes em uma mesma campanha

O código deverá ter suas classes, métodos e atributos comentados


## Requisitos

- NÃO utilizar frameworks
- PHP POO
- MySql ou MariaDB


## Estrutura

### Empresas

- CNPJ
- Razão Social
- Endereço
- Nome do responsável
- E-mail
- Telefone


### Campanhas

- Título
- Descrição
- Data de início
- Data de término


### Participantes

- CPF
- Nome completo
- E-mail



## Publicação
- Após finalizar, publicar o código em um repositório GIT público e nos enviar o link.


## Prazo de entrega
- 3 dias